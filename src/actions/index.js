import { createAction } from 'redux-actions';

export const GET_REFS_REQ = createAction('GET_REFS_REQ');
export const GET_REFS_SUCCESS = createAction('GET_REFS_SUCCESS');
export const GET_REFS_FAILURE = createAction('GET_REFS_FAILURE');
