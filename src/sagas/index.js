import { put, takeLatest, all } from 'redux-saga/effects';
import {GET_REFS_REQ, GET_REFS_SUCCESS, GET_REFS_FAILURE} from '../actions';

const a = { GET_REFS_SUCCESS, GET_REFS_FAILURE };
console.log(a);

const api_key = process.env.REACT_APP_API_KEY;
const api_base = process.env.REACT_APP_API_URL;

function *fetchRefs() {
    console.log(`fetching refs from ${api_base}/references`);

    const json = yield fetch(`${api_base}/references`, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${api_key}`
        }
    })
    .then(response => response.json());

    yield put({ type: 'GET_REFS_SUCCESS', json: json});
}

function *actionWatcher() {
    yield takeLatest(GET_REFS_REQ, fetchRefs);
}

export default function *rootSaga() {
    yield all([
        actionWatcher()
    ]);
}
