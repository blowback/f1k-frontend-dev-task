import React from 'react';
import ReactDOM from 'react-dom';
import createSagaMiddleWare from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { logger } from 'redux-logger';
import reducer from './reducers';
import rootSaga from './sagas';
import App from './components/App';
import {GET_REFS_REQ} from './actions';
import './index.css';

const sagaMiddleware = createSagaMiddleWare();

const store = createStore(
    reducer,
    applyMiddleware(sagaMiddleware, logger)
);

sagaMiddleware.run(rootSaga);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

store.dispatch(GET_REFS_REQ());
