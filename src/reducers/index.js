import { handleActions } from 'redux-actions';
import { GET_REFS_REQ, GET_REFS_SUCCESS, GET_REFS_FAILURE } from '../actions';

const defaultState = {};

const reducer = handleActions(
    {
        [GET_REFS_REQ]: state => ({...state, loading: true}),
        [GET_REFS_SUCCESS]: (state, action) => ({...state, refs: action.json, loading: false}),
        [GET_REFS_FAILURE]: (state, action) => ({...state, error: action.error}),
    },
    defaultState
);

export default reducer;
